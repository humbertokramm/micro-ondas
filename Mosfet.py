import matplotlib.pyplot as plt
from printSI import printSI
from LTSpiceData import readLTspicetable,guiaTangente,getDerivada,getIndex
#from math import atan

tecnologia = '10µm'

titulo = 'Curvas do Mosfet para '+tecnologia
titulo2 = 'Análise de VT para '+tecnologia
fileName = 'mos_curves'
fileName2 = 'mos_curves2'
escalay = 1e6
ti = 1.1 #Volts
to = 1.7 #Volts
W = 1e-6
L = 10e-6


def getVT(xValues,yValues):
	yMax = max(max(yValues))/1000
	i = getIndex(max(yValues),yMax)
	vt = xValues[0][i]
	return vt

##########################################################################
# Obtenção do VT
tablex, tabley, axes, label_var,label_value = readLTspicetable(fileName2)

#Busca o VT
V_t = getVT(tablex,tabley)

plt.figure()
plt.xlabel(axes[0]+'[V]')
plt.ylabel(axes[1]+'[µA]')

# Marcação do VT
a, b = guiaTangente(tabley,V_t,V_t)

ya = [j * escalay for j in a[1]]
yb = [j * escalay for j in a[1]]
plt.plot(a[0],ya,color='black',label = 'VT = '+str(V_t)+'V')
plt.plot(b[0],yb,color='black')

# desenha as curvas
for i in range (len(tablex)):
	legenda = label_var[i] + '= ' + str(label_value[i])+'V'
	y = [j * escalay for j in tabley[i]]
	plt.plot(tablex[i],y)#,label=legenda)
plt.grid(True, axis = 'both')
#Informações extra
plt.legend()
plt.title(titulo2)
plt.margins(0, 0.1)


##########################################################################
V_GS = []
λ = []
I_D = []

# Curvas de VGS
tablex, tabley, axes, label_var,label_value = readLTspicetable(fileName)

plt.figure()
plt.xlabel(axes[0]+'[V]')
plt.ylabel(axes[1]+'[uA]')
# desenha as curvas
for i in range (len(tablex)):
	legenda = label_var[i] + '= ' + str(label_value[i])+'V'
	y = [j * escalay for j in tabley[i]]
	plt.plot(tablex[i],y,label=legenda)
	V_GS.append(label_value[i])

plt.grid(True, axis = 'both')
#Informações extra
plt.title(titulo)
plt.legend()
plt.margins(0, 0.1)

##########################################################################


plt.figure()
plt.xlabel(axes[0]+'[V]')
plt.ylabel(axes[1]+'[µA]')
# Marcação da tangente
a, b = guiaTangente(tabley,ti,to)

ya = [j * escalay for j in a[1]]
yb = [j * escalay for j in a[1]]
plt.plot(a[0],ya,color='black')
plt.plot(b[0],yb,color='black')

# desenha as tangentes
for j in range(len(tablex)):
	dv,Va,yo_,index = getDerivada(tablex[j],tabley[j],ti,to)
	λ.append(-1/Va)
	I_D.append(tabley[j][index])
	legenda =  'slope = '+str(round(dv,1))+ '|λ = '+str(round(λ[-1],3))
	#legenda =  'slope = '+str(round(dv,1))+'| x = '+str(round(Va,1))+ '|λ = '+str(round(λ,1))
	#legenda = 'x = '+str(round(Va,1))
	plt.plot([Va,to],[0,yo_*escalay],label = legenda)#,color='black')

# desenha as curvas novamente
for i in range (len(tablex)):
	y = [j * escalay for j in tabley[i]]
	plt.plot(tablex[i],y)
plt.grid(True, axis = 'both')
#Informações extra
plt.title(titulo)
plt.legend()
plt.margins(0, 0.1)

##########################################################################

#trabalho = ''

#I_D = (1/2)*µ_N*C_OX*(W/L)*((V_GS-V_t)**2)*(1+λ*V_DS)

#trabalho += printSI(I_D,'I_D','µ','A',latexEQU = 'I_D = (1/2)*µ_N*C_OX*(W/L)*((V_GS-V_t)**2)*(1+ Δ * V_DS)')
#print(trabalho)


#trabalho += printSI(uNC_OX,'uNC_OX','µ','',latexEQU = 'uNC_OX = 2*I_D*L/(W*((V_GS-V_t)**2)*(1+Δ*V_DS))')
#print(trabalho)
##########################################################################

plt.figure()
plt.xlabel('Vds [V]')
plt.ylabel('uNCox [µA/V²]')
for i in range(len(λ)):

	uNC_OX = []
	V_DS = tablex[i]
	#print('I_D = ',I_D[i]/escalay,'lambda = ',λ[i],'V_GS = ',V_GS[i])
	if(V_GS[i]-V_t > 0):
		for j in range (len(V_DS)):
			uNC_OX.append(2*I_D[i]*L/(W*((V_GS[i]-V_t)**2)*(1+λ[i]*V_DS[j])) )
		
		# desenha as curvas
		legenda = 'Vgs = '+str(V_GS[i])+'V'
		y = [j * escalay for j in uNC_OX]
		plt.plot(V_DS,y,label=legenda)
	
plt.grid(True, axis = 'both')
#Informações extra
plt.title('uNCox para '+tecnologia)
plt.legend()
plt.margins(0, 0.1)

##########################################################################

plt.show()

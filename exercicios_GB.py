from printSI import printSI
from numpy import sqrt, arctan, degrees, log10, tan, pi

print('''
1a. Uma regra de projeto geral para enlaces de micro-ondas é de 55% de desobstrução da primeira zona
de Fresnel. Para um enlace de 1 km a 2,5 GHz, qual é o raio máximo da primeira zona de Fresnel?
''')

# Variáveis

desobst = 0.55
c = 299792458 # m/s
f = 2.5e9 # Hz
n = 1
D = 1e3 # m
d_1 = d_2 = D/2 

#Cálculo

λ = c/f
printSI(λ,'λ','m','m')

r_max = sqrt( (n * λ * d_1 * d_2)/(d_1 + d_2) )
printSI(r_max,'r_max','','m')

print('''
1b. Que desobstrução máxima é exigida para este sistema?
''')
desobst_max = r_max * desobst
printSI(desobst_max,'desobst_max','','m')

print('''
2. Para a geometria da figura abaixo determine: 
	(a) a perda devido à difração por gume de faca;
	(b) a altura necessária de um obstáculo, modelado por gume de faca, para que as perdas sejam de 6dB.
Considere que a frequência de operação é de 900 MHz.
''')
# Variáveis

c = 299792458 # m/s
f = 900e6 # Hz
T_D = 10e3 # m
T_H =  50 # m
R_D = 2e3 # m
R_H = 25 # m
H = 100 # m

#Cálculo

λ = c/f
printSI(λ,'λ','m','m')

β_T = arctan( (H - T_H)/T_D)
printSI(β_T,'β_T','','rad')

γ_T = arctan( (H - R_H)/R_D)
printSI(γ_T,'γ_T','','rad')


α = β_T + γ_T
printSI(α,'α','','rad')

V = α * sqrt( 2*T_D*R_D / (λ*(T_D+R_D)) )
printSI(V,'V','','')

Perdas_dB = 20*log10(0.225/V)
printSI(Perdas_dB,'Perdas_dB','','dB')

h_neces =  R_H * R_D / (T_D + R_D)
printSI(h_neces,'h_neces','','m')


print('''
3. Considere a figura abaixo. Se a potência do transmissor é 30 dBm com ganho de transmissão e
recepção igual a 10 dB e 3 dB respectivamente, usando a frequência de 1,5GHz.
a) Calcule a potência em W e em dBm recebida para a geometria gume de faca e compare com a
perda no espaço livre teórico para este caso. Qual é a perda correspondente a difração?

''')

# Variáveis

c = 299792458 # m/s
f = 1.5e9 # Hz
T_D = 1e3 # m
T_H =  50 # m
T_H2 =  100 # m
R_D = 2e3 # m
R_H = 50 # m
R_H2 = 100 # m
D_TR = T_D + 1e3 + R_D # m

#Cálculo

λ = c/f
printSI(λ,'λ','','m')

β_T = arctan( (T_H2 - T_H)/T_D)
printSI(β_T,'β_T','m','rad')

γ_T = arctan( (R_H2 - R_H)/R_D)
printSI(γ_T,'γ_T','m','rad')

α = β_T + γ_T
printSI(α,'α','m','rad')

T_D2 = D_TR * tan(γ_T) / (tan(γ_T)+tan(β_T))
printSI(T_D2,'T_D2','k','m')

R_D2 = D_TR - T_D2
printSI(R_D2,'R_D2','k','m')

V = α * sqrt( 2*T_D2*R_D2 / (λ*(T_D2+R_D2)) )
printSI(V,'V','','')

Perdas_dB = 20*log10(0.225/V)
printSI(Perdas_dB,'Perdas_dB','','dB')


print('''
b) O que mudaria se a frequência fosse de 2,4GHz?
''')

f = 2.4e9 # Hz
#Cálculo da B

λ = c/f
printSI(λ,'λ','','m')

β_T = arctan( (T_H2 - T_H)/T_D)
printSI(β_T,'β_T','m','rad')

γ_T = arctan( (R_H2 - R_H)/R_D)
printSI(γ_T,'γ_T','m','rad')

α = β_T + γ_T
printSI(α,'α','m','rad')

T_D2 = D_TR * tan(γ_T) / (tan(γ_T)+tan(β_T))
printSI(T_D2,'T_D2','k','m')

R_D2 = D_TR - T_D2
printSI(R_D2,'R_D2','k','m')

V = α * sqrt( 2*T_D2*R_D2 / (λ*(T_D2+R_D2)) )
printSI(V,'V','','')

Perdas_dB = 20*log10(0.225/V)
printSI(Perdas_dB,'Perdas_dB','','dB')

print('''
4. A cellular base station is to be connected to its Mobile Telephone Switching Office located 5 km
away. Two possibilities are to be evaluated: 
(1) a radio link operating at 28 GHz, with Gt = Gr = 25 dB;
(2) a wired link using coaxial line having an attenuation of 0.05 dB/m, with four 30 dB repeater
amplifiers along the line. If the minimum required received power level for both cases is the same,
which option will require the smallest transmit power? (Pozar, volume 5)
''')

c = 299792458 # m/s
f = 28e9 # Hz

#Cálculo

λ = c/f
printSI(λ,'λ','m','m')

PL_wireless = -40*log10(5000) + 25 + 20*log10(λ) - 30*log10(4*pi)
printSI(PL_wireless,'PL_wireless','','dB')

PL_coaxial = -0.05 * 5000 + 4*30
printSI(PL_coaxial,'PL_coaxial','','dB')

print('''
5. At a distance of 300 m from an antenna operating at 5.8 GHz, the radiated power density in the main
beam is measured to be 7.5 × 10−3 W/m2. If the input power to the antenna is known to be 85 W,
find the gain of the antenna. (Pozar, volume 5)
''')

c = 299792458 # m/s
f = 5.8e9 # Hz
Radiaton = 7.5e-3 # W/m2
P_in = 85 # W
D = 300 # m

#Cálculo

λ = c/f
printSI(λ,'λ','m','m')

Gain = Radiaton * ((4*pi*D)**2) / (P_in * λ**2)
printSI(Gain,'Gain','k','W')

G_dB = 20*log10(Gain)
printSI(G_dB,'G_dB','','dB')





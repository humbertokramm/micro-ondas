from printSI import eng2float

def guiaTangente(y=0,xi=0,xo=0):
	linha1 = [xi,xi],[min(min(y)),max(max(y))]
	linha2 = [xo,xo],[min(min(y)),max(max(y))]
	return linha1, linha2



def getLabelValue(label):
	var_ = ''
	value_ = 0
	
	start = label.find(':')+2
	end = label.find('=')
	var_ = label[start:end]
	
	start = label.find('=')+1
	end = label.find('(')
	value_ = eng2float(label[start:end])
	
	return var_, value_

def readLTspicetable(file):
	list =[]
	lb_var = []
	lb_value = []
	curvex = []
	curvey = []
	ax = 0
	tb_x = []
	tb_y = []
	f = open(file+'.txt')
	for l in f.readlines():
		list.append( l.strip().split('\t') )
		if(ax == 0):
			ax = list[-1]
		elif(list[-1][0][:4]=='Step'): 
			if(len(curvex)>0): tb_x.append(curvex)
			if(len(curvey)>0): tb_y.append(curvey)
			curvex = []
			curvey = []
			var, value = getLabelValue(list[-1][0])
			lb_var.append(var)
			lb_value.append(value)
		else:
			curvex.append(float(list[-1][0]))
			curvey.append(float(list[-1][1]))
	if(len(curvex)>0): tb_x.append(curvex)
	if(len(curvey)>0): tb_y.append(curvey)
	return tb_x, tb_y, ax, lb_var,lb_value

def getIndex(lista, value):
	k = 0
	if(value > max(lista)):
		print('too big')
		return 'nan'
	if(value < min(lista)):
		print('too small')
		return 'nan'
	for j in range(len(lista)):
		if(lista[j]<value): k = j
		if(lista[j]>value): return int(round(k+(j-k)/2,0))
	return 'nan'

def getDerivada(xL,yL,xi,xo):
	i = getIndex(xL,xi)
	o = getIndex(xL,xo)
	derivada = (yL[o] - yL[i])/(xL[o] - xL[i])
	xi_ = xL[o]-yL[o]/derivada
	return derivada,xi_,yL[o],i